import gensim
import string
import codecs
import logging
from nltk.tokenize import wordpunct_tokenize
from gensim.models import Word2Vec
from gensim.models import Phrases
from numpy import mean
from numpy import std


logging.basicConfig(format = u'[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s', level = logging.NOTSET)

	

class MyCents(object):
	def __init__(self, fis):
		self.fis = fis
	
	@staticmethod
	def process_line(line):
		result = ''
		for ch in line:
			if ch in string.punctuation or ch.isdigit():
				result += ' '
			else:
				result += ch
		return result
	
	def __iter__(self):
		with codecs.open(self.fis, 'r') as fin:
			for line in fin:
				#line = ''.join([i for i in line if i not in string.punctuation and not i.isdigit()])
				line = MyCents.process_line(line)
				#yield wordpunct_tokenize(line)
				yield line.split()

def find_average_length_of_lines(fis):
	print 'counting length'
	linelength = []
	with codecs.open(fis, 'r') as fin:
		for line in fin:
			linelength.append(len(MyCents.process_line(line).split()))
	print 'done'
	'''
	return the mean + standard deviation of the length of all lines
	'''
	return int(mean(linelength) + std(linelength))

def train_w2v_unigram(fishier, wind=111):
	print 'Training unigrams...'
	sentences = MyCents(fishier)
	model = Word2Vec(sentences, size=512, workers=10, window=wind, min_count=1, max_vocab_size=None)
	print 'Saving...'
	model.save('mix_unigram.model')
	print 'Done'


def train_w2v_bigram(fishier, wind=111):
	print 'Training bigrams...'
	sentences = MyCents(fishier)
	print 'Making phrases...'
	bigram_transformed = Phrases(sentences)
	print 'Training...'
	model = Word2Vec(bigram_transformed[sentences], size=512, workers=10, window=wind, min_count=1, max_vocab_size=None)
	print 'Saving...'
	model.save('mix_bigram.model')
	print 'Done'

def train_w2v_trigram(fishier, wind=111):
	print 'Training trigrams...'
	sentences = MyCents(fishier)
	print 'Making phrases...'
	bigram = gensim.models.Phrases(sentences)
	trigram = Phrases(bigram[sentences])
	print 'Training...'
	model = Word2Vec(trigram[sentences], size=512, workers=10, window=wind, min_count=1, max_vocab_size=None)
	print 'Saving...'
	model.save('mix_trigram.model')
	print 'Done'

def train_w2v_fourgram(fishier, wind=111):
	print 'Training 4-grams...'
	sentences = MyCents(fishier)
	print 'Making phrases...'
	bigram = gensim.models.Phrases(sentences)
	trigram = Phrases(bigram[sentences])
	fgram = Phrases(trigram[sentences])
	print 'Training...'
	model = Word2Vec(fgram[sentences], size=512, workers=10, window=wind, min_count=1, max_vocab_size=None)
	print 'Saving...'
	model.save('mix_fgram.model')
	print 'Done'

fis = 'europarl-v7.fr-en.merge'
#fis = 'test.data'
#wind = find_average_length_of_lines(fis)
#print 'setting window '+str(wind)
train_w2v_unigram(fis)
train_w2v_bigram(fis)
train_w2v_trigram(fis)
train_w2v_fourgram(fis)