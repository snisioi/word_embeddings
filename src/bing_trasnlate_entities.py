import gensim
import string
import codecs
import logging
from nltk.tokenize import wordpunct_tokenize
from gensim.models import Word2Vec
from bs4 import BeautifulSoup as BS

import translate as translator
translator.set_credentials(app_id='europarltranstest',client_id='europarltranstest',client_secret='AOnbISxwUxOlj835Fs+hTOufnoJR3i8E6QWB/ll47ME=')

logging.basicConfig(format = u'[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s', level = logging.NOTSET)
class MyCents(object):
	def __init__(self, fis):
		self.fis = fis
	def __iter__(self):
		with codecs.open(self.fis, 'r') as fin:
			for line in fin:
				line = ''.join([i for i in line if i not in string.punctuation and not i.isdigit()])
				yield wordpunct_tokenize(line)


def train_w2v():
	print 'Training...'
	sentences = MyCents('europarl-v7.fr-en.merge')
	model = Word2Vec(sentences, size=200, workers=4, window=100)
	print 'Saving...'
	model.save('/tmp/text8.model')
	print 'Done'

def get_bing_translations_with_cache(fisin, fisout, cache):
	# on each line we have an extracted xml tag with an entity
	logging.info('Translating...')
	counter = 0
	already_done = dict()
	with codecs.open(cache,'r') as fc:
		for line in fc:
			parts = line.split('\t')
			already_done[parts[0]] = parts[1].strip()

	with codecs.open(fisin, 'r') as fin:
		with codecs.open(fisout, 'w', 'utf-8') as fout:
			for line in fin:
				counter += 1
				text = BS(line.strip()).text
				if text in already_done:
					translated_text = already_done[text]
				else:	
					translated_text = translator.translate(text, 'en', 'fr')
				try:
					fout.write(text+'\t'+translated_text.decode('utf-8')+'\n')
				except:
					print translated_text
				if counter % 1000 == 0:
					logging.info('Translated '+str(counter)+ ' lines ')
	logging.info('Done')

def get_bing_translations(fisin, fisout):
	# on each line we have an extracted xml tag with an entity
	logging.info('Translating...')
	counter = 0
	with codecs.open(fisin, 'r') as fin:
		with codecs.open(fisout, 'w', 'utf-8') as fout:
			for line in fin:
				counter += 1
				text = BS(line.strip()).text
				translated_text = translator.translate(text, 'en', 'fr')
				try:
					fout.write(text+'\t'+translated_text.decode('utf-8')+'\n')
				except:
					print translated_text
				if counter % 1000 == 0:
					logging.info('Translated '+str(counter)+ ' lines ')
	logging.info('Done')

get_bing_translations('europarl-v7.fr-en.en.ORGANIZATION','europarl-v7.fr-en.TRANS.ORGANIZATION')
