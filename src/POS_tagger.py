'''
A multi lingual POS tagger
'''
import gensim
import string
import codecs
import logging
import nltk
from itertools import izip
from nltk.tokenize import wordpunct_tokenize
from gensim.models import Word2Vec
from gensim.models import Phrases
from numpy import mean
from numpy import std

logging.basicConfig(format = u'[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s', level = logging.NOTSET)

class MyCents(object):
	def __init__(self, fis):
		self.fis = fis
	@staticmethod
	def process_line(line):
		result = ''
		for ch in line:
			if ch in string.punctuation or ch.isdigit():
				result += ' '
			else:
				result += ch
		return result
	def __iter__(self):
		with codecs.open(self.fis, 'r', 'utf-8') as fin:
			for line in fin:
				#line = ''.join([i for i in line if i not in string.punctuation and not i.isdigit()])
				line = MyCents.process_line(line)
				#yield wordpunct_tokenize(line)
				yield line.split()

'''
this class returns the POS taggs of the English sentences
'''
class CentsEN(MyCents):
	def __init__(self, fis):
		MyCents.__init__(self, fis)
	def __iter__(self):
		with codecs.open(self.fis, 'r', 'utf-8') as fin:
			for line in fin:
				line = MyCents.process_line(line)
				yield [pos for _,pos in nltk.pos_tag(line.split())]

class JoinPosWds(object):
	def __init__(self, en_fis, other_fis):
		self.en_fis = en_fis
		self.other_fis = other_fis
		self.sents_en_fis = CentsEN(en_fis)
		self.sents_other_fis = MyCents(other_fis)
	def __iter__(self):
		for pos_tags, words in zip(self.sents_en_fis, self.sents_other_fis):
			yield pos_tags + words

def POS_tag_file(fis, fis_out):
	logging.info('POS tagging')
	counter = 0
	with codecs.open(fis_out,'w', 'utf-8') as fout:
		with codecs.open(fis, 'r', 'utf-8') as fin:
			for line in fin:
				counter += 1
				line = MyCents.process_line(line)
				fout.write(' '.join([pos for _,pos in nltk.pos_tag(line.split())]))
				if counter % 1000 == 0:
					logging.info('Processed '+ str(counter)+' lines') 


def find_average_length_of_lines(fis):
	print 'counting length'
	linelength = []
	with codecs.open(fis, 'r') as fin:
		for line in fin:
			linelength.append(len(MyCents.process_line(line).split()))
	print 'done'
	'''
	return the mean + standard deviation of the length of all lines
	'''
	return int(mean(linelength) + std(linelength))


def joint_train(EN_file, Other_file, wind=111):
	print 'Training unigrams POS...'
	sentences = JoinPosWds(EN_file, Other_file)
	model = Word2Vec(sentences, size=256, workers=10, window=wind, min_count=1, max_vocab_size=None)
	print 'Saving...'
	model.save('POS_unigram_joint.model')
	print 'Done'


'''
if the POS tags are already extracted and joined, use this functions
'''
def train_POS_unigram(fishier, wind=111):
	print 'Training unigrams...'
	sentences = MyCents(fishier)
	model = Word2Vec(sentences, size=256, workers=10, window=wind, min_count=1, max_vocab_size=None)
	print 'Saving...'
	model.save('POS_unigram.model')
	print 'Done'


#fis_en = 'europarl-v7.fr-en.en'
#fis_fr = 'europarl-v7.fr-en.fr'
#fis_en = 'en_test.data'
#fis_fr = 'fr_test.data'
#POS_tag_file(fis_en, fis_en+'.POS')
#train(fis_en, fis_fr)

fis = 'europarl-v7.fr-en.fr.POS.merge'
train_POS_unigram(fis)