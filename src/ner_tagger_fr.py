# -*- coding: utf-8 -*-
import os
import sys
import codecs
import string
import logging
from nltk.tokenize import WhitespaceTokenizer
import nltk 
import re
from jsonrpclib.jsonrpc import ServerProxy
from pprint import pprint

logging.basicConfig(format = u'[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s', level = logging.NOTSET)


counter = 0
def log_counter_message():
	global counter
	counter += 1
	if counter % 1000 == 0:
		logging.info('Processed '+str(counter)+' lines')

'''
tag_NER is a method registered on the server
'''

class OpenNLP:
	def __init__(self, host='localhost', port=8080):
		uri = "http://%s:%d" % (host, port)
		self.server = ServerProxy(uri)
	@staticmethod
	def process_line(line):
		result = ''
		for ch in line:
			if ch in string.punctuation:
				result += ' '+ch+' '
			else:
				result += ch
		return result
	def get_NER(self, text):
		return self.server.tag_NER(text)
	def get_stanford_NER(self, text):
		response = self.get_NER(OpenNLP.process_line(text))
		#only_these_ones=['organization', 'location']
		#which_ones = set(only_these_ones)
		found = re.findall('START:(.*?)<END',response)
		for entity in found:
			parts = entity.split('>')
			tip = parts[0].strip()
			ent = parts[1].strip()
			#if tip in which_ones:
			response = response.replace(entity,'<'+tip+'>'+ent+'</'+tip+'>')
		response = response.replace('<START:',' ').replace('<END>',' ')
		return response

'''
French NER using opennlp as server
first run:
	python ~/Downloads/training/src/opennlp/opennlp_NER_server.py
'''
def anotate_NER_in_file(fis_in, fis_out):
	nlp = OpenNLP()
	logging.info('Annotating corpus...')
	global counter
	counter= 0
	cc_counter = 0
	already = set()
	if os.path.exists(fis_out):
		with codecs.open(fis_out, 'r','utf-8-sig') as fout:
			for line in fout:
				cc_counter += 1

	with codecs.open(fis_in,'r', 'utf-8-sig') as fisin:
		with codecs.open(fis_out, 'a','utf-8-sig') as fout:
			for sentence in fisin:
				log_counter_message()
				cc_counter -= 1
				#annotated_sentence = get_stanford_NER_for_sentence(sentence, ['ORGANIZATION', 'LOCATION'])
				#results = nlp.get_NER(sentence)
				if cc_counter < 0:
					results = nlp.get_stanford_NER(sentence)
					fout.write(results+'\n')

#nlp = OpenNLP()
#print nlp.get_stanford_NER("je m appelle Sergiu et je suis en Paris l UE et Nation Unies par conséquent, afin de pouvoir garantir une sécurité juridique, \n")

anotate_NER_in_file('../europarl-v7.fr-en.fr','../europarl-v7.fr-en.fr.annotated')	
