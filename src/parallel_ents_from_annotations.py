import logging
from itertools import izip
from bs4 import BeautifulSoup as bs

logging.basicConfig(format = u'[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s', level = logging.NOTSET)


fis1 = 'europarl-v7.fr-en.en.annotated'
fis2 = 'europarl-v7.fr-en.fr.annotated'

counter = 0

with open(fis1) as f1, open(fis2) as f2: 
    for x, y in izip(f1, f2):
    	counter += 1
    	if counter % 1000 == 0:
    		logging.info('Processed '+str(counter)+' limes') 
    	ents1 = bs(x, 'html')
    	orgs1 = ents1.findAll('organization')
    	locs1 = ents1.findAll('location')
    	
    	ents2 = bs(y, 'html')
    	orgs2 = ents2.findAll('organization')
    	locs2 = ents2.findAll('location')

    	