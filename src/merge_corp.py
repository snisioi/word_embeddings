import codecs
with codecs.open ('europarl-v7.fr-en.en', 'r') as en:
	with codecs.open('europarl-v7.fr-en.fr','r') as fr:
		with codecs.open('europarl-v7.fr-en.merge','w') as fout:
			while True:
				l1 = en.readline()
				if not l1:
					break
				l2 = fr.readline()
				if not l2:
					break
				fout.write(l1.strip() + ' '+ l2.strip()+'\n')