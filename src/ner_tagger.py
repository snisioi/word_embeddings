# coding: utf-8
import os
import sys
import codecs
import logging
from nltk.tokenize import WhitespaceTokenizer
import nltk 

import ner

counter = 0
tokenize_function = nltk.word_tokenize
sentence_tokenizer = nltk.data.load('tokenizers/punkt/english.pickle').tokenize
logging.basicConfig(format = u'[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s', level = logging.NOTSET)


def log_counter_message():
    global counter
    counter += 1
    if counter % 1000 == 0:
        logging.info('Processed '+str(counter)+' lines')


def get_stanford_NER_for_sentence(sentence, which_entities=['ORGANIZATION', 'LOCATION', 'PERSON']):
    #cd /home/snisioi/standford-nlp/stanford-ner-2014-10-26/
    #java -mx2000m -cp stanford-ner.jar edu.stanford.nlp.ie.NERServer -loadClassifier classifiers/english.conll.4class.distsim.crf.ser.gz -port 8080
    tagger = ner.SocketNER(host='localhost', port=8080, output_format="slashTags")
    try:
        tagger.get_entities("Hi, Mr. John smith from Stanford University would like to visit Amsterdam ")
    except:
        logging.error('stanford NER server not running on port 8080' )
        print ('cd /home/snisioi/standford-nlp/stanford-ner-2014-10-26/')
        print ('java -mx2000m -cp stanford-ner.jar edu.stanford.nlp.ie.NERServer -loadClassifier classifiers/english.conll.4class.distsim.crf.ser.gz -port 8080')
        sys.exit(-1)
    ents = tagger.get_entities(sentence)
    done = set()
    for tip in which_entities:
        for elem in ents.get(tip,[]):
                if elem in done:
                    continue
                sentence=sentence.replace(elem,'<'+tip+'>'+elem+'</'+tip+'>' )
                done.add(elem)
    return sentence


def anotate_NER_in_file(fis_in, fis_out):
    logging.info('Annotating corpus...')
    global counter
    counter= 0
    with codecs.open(fis_in,'r', 'utf-8-sig') as fisin:
        with codecs.open(fis_out, 'w','utf-8-sig') as fout:
            for sentence in fisin:
                log_counter_message()
                annotated_sentence = get_stanford_NER_for_sentence(sentence, ['ORGANIZATION', 'LOCATION'])
                fout.write(annotated_sentence)

def main():
    #different task
    fin = '/home/snisioi/Downloads/training/europarl-v7.es-en.en'
    fout = '/home/snisioi/Downloads/training/europarl-v7.es-en.en.annotated'
    anotate_NER_in_file(fin, fout)
    
    logging.info('Done')

if __name__ == '__main__':
    main()
