# -*- coding: utf-8 -*-

from jsonrpclib.jsonrpc import ServerProxy
from pprint import pprint
from bs4 import BeautifulSoup as bs


class OpenNLP:
    def __init__(self, host='localhost', port=8080):
        uri = "http://%s:%d" % (host, port)
        self.server = ServerProxy(uri)

    def get_NER(self, text):
        return self.server.tag_NER(text)

if __name__ == '__main__':
    nlp = OpenNLP()
    # don't forget to add a new line
    results = nlp.get_NER("Je m'appelle Sergiu et je suis roumain dans le Bucarest \n")
    print(results)
